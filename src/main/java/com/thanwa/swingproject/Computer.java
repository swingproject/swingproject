/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanwa.swingproject;

import java.util.concurrent.ThreadLocalRandom;

/**
 *
 * @author tud08
 */
public class Computer {

    private int hand;
    private int playerhand;
    private int win, lose, draw;
    private int status;

    public Computer() {
    }

    public int choob() {
        return ThreadLocalRandom.current().nextInt(0, 3);
    }

    public int paoYingChoob(int playerhand) {
        this.playerhand = playerhand;
        this.hand = choob();
        if (this.playerhand == this.hand) {
            draw++;
            status = 0;
            return 0;
        }
        if (this.playerhand == 0 && this.hand == 1) {
            win++;
            status = 1;
            return 1;
        }
        if (this.playerhand == 1 && this.hand == 2) {
            win++;
            status = 1;
            return 1;
        }
        if (this.playerhand == 2 && this.hand == 0) {
            win++;
            status = 1;
            return 1;
        }
        lose++;
        status = -1;
        return -1;
    }

    public int getHand() {
        return hand;
    }

    public int getPlayerhand() {
        return playerhand;
    }

    public int getWin() {
        return win;
    }

    public int getLose() {
        return lose;
    }

    public int getDraw() {
        return draw;
    }

    public int getStatus() {
        return status;
    }
}
